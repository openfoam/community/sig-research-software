# Archive of talks

This is an archive of talk slides and related material,
in particular content that is not archived elsewhere.
More material is available on the [wiki](https://wiki.openfoam.com/Research_Software_Engineering_Special_Interest_Group).
