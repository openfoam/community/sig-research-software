# Containers for OpenFOAM

## Reasons for Containerization

There are a number of reasons, why you might want to containerize OpenFOAM.
We will just list some highlights:
- reproducibility
- portability
- dependency management
- archivability
- integration into CI/CD
- lower runtime overhead in comparison to virtual machines
- some don't require root access
- no need to install a lot of things on a machine (the container software of course)

## Apptainer

[Apptainer](https://apptainer.org/) is written for HPC and does not need root access.
It therefore is available on most clusters.
In contrast to Docker, Apptainer has image files, what makes it very intuitive and easy to handle.
It could be added to git, uploaded to an archive (zenodo,...) or sent via e-mail or saved on a USB-stick.

The idea is, to provide a Apptainer image and this can run your OpenFOAM case on (almost) any machine.


In the meeting on the 18th of Oct. 2024, Mohammed Eldwardi Fadeli (TUDa, NHR4CES) gave a presentation called "Apptainer containers for OpenFOAM".

Links:
- [slides](https://foamscience.github.io/openfoam-apptainer-containers-presentation/)
- [related repository](https://github.com/FoamScience/openfoam-apptainer-packaging)


## Other containerization solutions

This list might be incomplete:
- docker
- podman
- SARUS
