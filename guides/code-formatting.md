# Automatic code formatting

This guide was started during the OpenFOAM RSE meeting of February 16, 2024.
However, it is not meant to be static: Contribute your knowledge via a merge request!

## Status quo

OpenFOAM does not yet offer a specification for any automatic code formatting tool. However, several people and derived projects have tried to provide a clang-format specification.

We are collaboratively trying to find a solution in the [`automatic-formatting`](https://gitlab.com/openfoam/community/sig-research-software/-/tree/main/automatic-formatting) directory. Please contribute there.
