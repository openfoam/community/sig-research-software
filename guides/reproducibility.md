# Reproducibility for OpenFOAM codes and cases

This guide was started during the OpenFOAM RSE meeting of September 29, 2023.
However, it is not meant to be static: Contribute your knowledge via a merge request!

## Reproducibility at the core of the OpenFOAM Journal

The [OpenFOAM Journal](https://journal.openfoam.com/) puts an emphasis on reproducibility. Authors are required to submit their code, and the reviewers are asked to execute it and cross-check the results. In case the reviewers do not do this, the task falls back to the hands of the editor. A code review is not expected.

The code is uploaded as an archive next to the manuscript on the webpage of the journal. While the cases are archived, they are not integrated into any regression testing system. Their execution is also not necessarily automated.

To safeguard the reproducibility of the results, we make the following initial recommendation to the editorial board:

- Distinguish between the scientific and the reproducibility review, making a reproducibility review an explicit step.
- Since the reproducibility review may need quicker iterations and technical guidance, we recommend keeping it public, or at least with an appointed reviewer with visible identity. The scientific review can remain double-blind.
- The reproducibility review can take one of the following paths:
   - A) It happens during the normal review period, by a member of the editorial board under a confidentiality agreement. This reflects the current state.
   - B) It happens after the normal review period, and once the paper and code have been published, in the code hosting platform of the respective project. As a result of a successful review, the publication gets a "reproducibility badge" and potentially new revisions of the code/case archives and of the manuscript. The review happens by the community, under the guidance of a member of the editorial (advisory) board, similar to the [JOSS reviews on GitHub](https://joss.readthedocs.io/en/latest/reviewer_guidelines.html).
- Ideally, the result of the review would be a fully-automated, containerized workflow to execute the code and check the results.
- After passing the (extensive) reproducibility review, the case can, in principle, be integrated into a regression testing system (to be determined).

Important factor is to not overload the reviewers and the editors, or extend the review process. With our suggestion (B):

- The reviewers/editor still do a basic check that the important information is available, but do not need to do extensive checks, which may delay the publication process.
- The author has intrinsic motivation of visibility, quality improvement, and integration into a regression testing system.
- New members of the community can more easily be integrated, since the reproducibility review does not require domain expertise.

We will submit this recommendation to the editors.

## Common issues that hinder reproducibility

- OpenFOAM version not listed in full. In particular, bugfix revisions. This is difficult to detect by the user, should be required in publications, and could be better be communicated via the versioning system (e.g., v2306.1) and the logs.
- Compiler version or dependencies not listed.
- Mesh generation tools relying on random number generators. This can become an issue particularly when changing the number of parallel subdomains.

## Regression testing and parameter exploration

We identified the lack of a commonly-accepted regression testing workflow for OpenFOAM as a important barrier to achieving automated reproducibility checks.

Related tools discussed in our group include:

- [Snakemake](https://snakemake.github.io/): "A framework for reproducible data analysis"
- [DVC](https://dvc.org/doc/user-guide): "Data Version Control is a free, open-source tool for data management, ML pipeline automation, and experiment management."
- [oftest](https://pypi.org/project/oftest/) (by Henning Scheuffler): "test framework for OpenFOAM"
   - See an [example integration on GitHub Actions](https://github.com/DLR-RY/TwoPhaseFlow/blob/of2206/.github/workflows/openfoam.yml) and the respective [test script](https://github.com/DLR-RY/TwoPhaseFlow/blob/of2206/testsuite/cht/fixedFlux/test_chtFixedFlux.py).
- [ReFrame](https://reframe-hpc.readthedocs.io/): "a powerful framework for writing system regression tests and benchmarks, specifically targeted to HPC systems."
- [fieldcompare](https://gitlab.com/dglaeser/fieldcompare): "a Python package with command-line interface (CLI) that can be used to compare datasets for (fuzzy) equality."

We intend to continue the discussion in depth in our next meeting.

## Example: Regression testing at the SFB1194 (TUDa)

On December 7, 2023, Moritz Schwarzmeier presented a continuous integration framework for reproducible research.

Based on GitLab CI, the framework performs the following checks for each code published with a paper:

- **pre-checks** for the [Citation File Format](https://citation-file-format.github.io/) schema using the official [validator Python script](https://github.com/citation-file-format/citation-file-format/tree/main/examples).
- **build a container** with specified OpenFOAM-version and needed packages (e.g. matplotlib, numpy, pandas,jupyter-notebook, PyFoam) and store it in a container registry.  
This should only be done, when necessary.
- **compile** the code inside the container
- **performs simulations** and prepares reports, based on Jupyter notebooks and Python scripts that check the correctness of the results (e.g. expected velocity)
- **performs parameter studies** and prepares reports, based on Jupyter notebooks and Python scripts that check the correctness of the results (e.g. order of convergence)

The last two steps also produce plots, that can be downloaded from the CI and integrated into the paper.

The demonstrated example GitLab CI file (part of a [currently private repository](https://gitlab.com/interface-tracking/twoPhaseInterTrackFoam)) is distributed into multiple files with the [`include:` keyword](https://docs.gitlab.com/ee/ci/yaml/#include), also wrapped under the [`trigger:` keyword](https://docs.gitlab.com/ee/ci/yaml/#trigger).
Multiple jobs run parallel for three different OpenFOAM versions (three most recent OpenCFD), utilizing the keyword [`parallel:matrix`](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix).
