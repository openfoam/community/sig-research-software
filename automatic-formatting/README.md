# An attempt to automatic formatting of OpenFOAM code

This repository serves as a minimal working example to experiment with various options for automatic code formatting / indentation in OpenFOAM. Tweak the settings or add configuration files for your favorite tool via a pull request. The smaller the diff of the example file, the better.

Many people have already tried to find a solution. See the [central issue in the OpenFOAM GitLab](https://develop.openfoam.com/Development/openfoam/-/issues/1634).

Ideally, the provided solution should follow the [OpenFOAM Style Guide](https://develop.openfoam.com/Development/openfoam/-/wikis/coding/style/style) as much as possible.

Example code files:

- (`dynamicIndexedOcttree.C`), which is [part of OpenFOAM](https://develop.openfoam.com/Development/openfoam/-/blob/a9b451b3e4f98933b8b3f7df13066f5537de5086/src/OpenFOAM/algorithms/dynamicIndexedOctree/dynamicIndexedOctree.C), licensed under GPLv3.
- (`foamDictionary.C`), which is part of OpenFOAM-v2312 (`applications/utilities/miscellaneous/foamDictionary/foamDictionary.C`), licensed under GPLv3.
- (`twoPhaseMixture.H`), which is part of OpenFOAM-v2312 (`src/transportModels/twoPhaseMixture/twoPhaseMixture/twoPhaseMixture.H`, still licensed under GPLv3.
- (`interfaceTrackingFvMesh.C`), which is [part of twoPhaseInterTrackFoam](https://gitlab.com/interface-tracking/twophaseintertrackfoamrelease), also GPLv3.

## How to compare files

There is a `Allrun` script you can use.
If you are proposing a new feature, use `Allrun feat`.
It will format the files from the `example_code` folder into `example_code_formatted` or `example_code_proposed`, if you run `Allrun feat`.
The first folder is for the original code.
The second folder is for the formatted code, as it is in the master branch.
The third folder is for the proposed change in your feature branch.

The script will run `meld` to compare the three versions automatically.

Make a screenshot like this, to discuss in issues or merge requests.
This example shows a change in the `AccessModifierOffset`.

![3-way-comparison](/Screenshot_threeWay-comparison.png)

The original formatting is on the left,
the version from the master branch is in the middle and
the version/change to be discussed is on the right.

## Contribute

Open an [issue](https://gitlab.com/openfoam/community/sig-research-software/-/issues) describing what you would like to see changed, including diffs/images of examples.

If you want to change some option, submit a [merge request](https://gitlab.com/openfoam/community/sig-research-software/-/merge_requests). You can fork the repository, or you can ask to be added directly here, if you want to work more actively. In the merge request, change the `.clang-format` file and modify the `*.formatted` files, so that we can keep track of what each option changes.

## FAQ

### Why is automatic formatting important?

Multiple reasons:

- Reduce merge conflicts
- Save developers' time, energy, and focus (OpenFOAM or derived projects)
- Make contributing much easier
- Stop worrying and love the ~~bomb~~ formatter

This is a very important step towards encouraging and being able to merge community contributions.

### What kind of automation are we talking about?

Examples:

- You push to GitLab and the CI tells you immediately if the code is correctly formatted or not.
- Before you make a commit, a Git pre-commit hook tells you if you are trying to commit incorrectly formatted code, or even formats the code for you.
- Any editor/IDE automatically formats on save, based on the common config.

### Do we need to stick to the existing style guide?

Ideally, yes. But the main reason is familiarity.

Potentially in-house tools that assume a specific format could also break (hypothetical scenario - are there any?).

### Will there be merge conflicts?

When first adding a formatter, there will probably be a large diff. Even if we perfectly follow the style guide, the formatter may "fix" existing inconsistencies. But every following contribution should simply be formatted with the same script.

Existing merge requests could just apply the same format automatically before merging. Updating the format will render existing review comments on merge requests as outdated. For this reason, the ideal scenario is that any major pull requests are merged before the codebase is first reformatted.

## Option: Clang-format

You can get recent versions of clang-format from PIP: `pip3 install --user clang-format==17.0.6`.  
You can also use `apt-get install clang-format-18`.

Format using:

```shell
cd example_code
clang-format dynamicIndexedOctree.C > dynamicIndexedOctree.C.formatted
```

[Clang-Format Style Options](https://clang.llvm.org/docs/ClangFormatStyleOptions.html)

Related discussions:

- [Pull request in OpenFOAM-preCICE](https://github.com/precice/openfoam-adapter/pull/173), where the included `.clang-format` configuration file is extracted from (GPLv3).
- [Invitation to the LLVM community](https://discourse.llvm.org/t/showcasing-the-strengths-of-clang-format-to-the-openfoam-community/75762)

See some [open issues](https://gitlab.com/openfoam/community/sig-research-software/-/issues).

## Option: c-mode

Mark Olesen maintains an OpenFOAM indentation rule in a [fork of the jed editor](https://github.com/olesenm/jed/blob/master/lib/cmode.sl#L1721). See [more details](https://develop.openfoam.com/Development/openfoam/-/issues/1634#note_64127). This has been so far rejected as a direction we should pursue as a general solution.
